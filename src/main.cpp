/*
 * Sample program for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 *
 * モデムとデバッグ用シリアルをつなげて直接操作できるようにする
 *
 */

#include <docodemo.h>
#include <SlrModem.h>

const uint8_t DEVICE_EI = 0x1;
const uint8_t DEVICE_GI = 0x02;
const uint8_t DEVICE_DI = 0x00; //00 is boradcast
const uint8_t CHANNEL = 0x10;

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

void setup() {
  Dm.begin();

  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, ON);

  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);

  while (!UartModem)
    ;
  Dm.ModemPowerCtrl(ON);
  vTaskDelay(150 / portTICK_PERIOD_MS);

#if 0
  modem.Init(UartModem, nullptr);

  //if you turn off radio power, set again or select true to write setting.
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);
#endif
}

void loop() {
  SerialDebug.println("start loop");
  while (1)
  {
    if (SerialDebug.available())
    {
      uint8_t data = SerialDebug.read();
      SerialDebug.write(data); // echo back
      UartModem.write(data);
    }

    if (UartModem.available())
    {
      SerialDebug.write(UartModem.read());
    }

    delay(2);
  }
}