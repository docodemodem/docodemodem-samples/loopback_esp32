/**********************************************************************/
/*
*	@Copyright	CIRCUIT DESIGN.INC. 2016 All rights reserved.
*
*	@file		$Workfile: Board.h $
*
*	@brief		Header to define list of constants.
*
*	@author		CIRCUIT DESIGN,INC.
*
*	@note      
*/
/**********************************************************************/
#ifndef _BOARD_H_
#define _BOARD_H_

/*======================================================================
 *  Include File 
 *====================================================================*/


/*======================================================================
 *	#define for Global
 *====================================================================*/

#define RADIO_UART_RX_PORT          (34) //(6) //To TX of modeule
#define RADIO_UART_TX_PORT          (17) //(7) //To Rx of modeule

#define SLR_BAUDRATE        (19200)
#define UART_BUFFER_SIZE      (128)
#define RX_BUFFER_SIZE         (20)
#define RED_LED 9
#define GREEN_LED 10

//UART0 is for debug
#define GPS_UART_NO 1   //UART1
#define RADIO_UART_NO 2 //UART2

#define GPS_UART_TX_PORT (33) //39 //CN6-4
#define GPS_UART_RX_PORT (39) //36 //CN6-3

#define SLR_BAUDRATE        (19200)
#define UART_BUFFER_SIZE      (128)
#define RX_BUFFER_SIZE         (20)

#define INTERNAL_SCL (22)
#define INTERNAL_SDA (21)
#define EXTERNAL_SCL (26)
#define EXTERNAL_SDA (27)

#define BME280_I2C_ADD (0x76)
#define AD5693_I2C_ADD (0x4C)  //DA
#define MCP3425_I2C_ADD (0x68) //AD
#define ADP5589_I2C_ADD (0x34)

#define TRUE                    (1)
#define FALSE                   (0)

#endif // _BOARD_H_

