/**********************************************************************/
/*
*	@Copyright	CIRCUIT DESIGN.INC. 2016 All rights reserved.
*
*	@file		$Workfile: Board.h $
*
*	@brief		Header to define list of constants.
*
*	@author		CIRCUIT DESIGN,INC.
*
*	@note      
*/
/**********************************************************************/
#ifndef _EXGPIO_H_
#define _EXGPIO_H_

#include "ADP5589.h"

void vLed_set(uint8_t led, uint8_t onoff);
void vRadioPower_set(uint8_t onoff);
void vGpsPower_set(uint8_t onoff);
bool exGPIO_init(TwoWire *theWire);
void vBeepVolume_set(uint8_t vol);
uint8_t ReadDipSW();

#endif